<?php

/**
 * Created by PhpStorm.
 * User: lincg
 * Date: 11/8/16
 * Time: 11:07 AM
 */
class Game extends CI_Controller
{
    const appKey = '';
    const appId  = 1;

    /**
     * 渠道用户玩某一个游戏
     * @param $gid
     */
    public function playChannel($gid){
        $params = array(
            'userId' => 1,
            'userName' => '',
            'userImg' => '',
            'userSex' => '',
            'channelExt' => '',
            'time' => time(),
        );
        $params['sign'] = $this->playSign($params);
        $gameUrl = $this->getGameUrl($gid);
        redirect($gameUrl.'/?'.http_build_query($params));
    }

    /**
     * 渠道的支付
     * 转入支付中心
     */
    public function pay(){
        $userId = $this->input->get('userId');
        $userName = $this->input->get('userName');
        $gameId = $this->input->get('gameId');
        $goodsId = $this->input->get('goodsId');
        $goodsName = $this->input->get('goodsName');
        $money = $this->input->get('money'); //单位是元,float
        $egretOrderId = $this->input->get('egretOrderId');
        $channelExt = $this->input->get('channelExt');
        $ext = $this->input->get('ext');
        $gameUrl = $this->input->get('gameUrl');
        $time = $this->input->get('time');
        $sign = $this->input->get('sign');
        if($this->verifyPaySign($egretOrderId, $gameId, $goodsId, $money, $time, $userId, $sign)){
            //todo 生成一个新的订单
            $orderId = $this->createOrder();
            redirect(site_url('/pay/req/'.$orderId));
        }
    }

    protected function createOrder(){

    }

    protected function verifyPaySign($egretOrderId, $gameId, $goodsId, $money, $time, $userId, $sign){
        return $sign == md5('appId='.self::appId.'egretOrderId='.$egretOrderId.'gameId='.$gameId.'goodsId='.$goodsId.'money='.$money.'time='.$time.'userId='.$userId);
    }

    /**
     * 渠道用户玩游戏生成校验字段
     * @param $params
     * @return string
     */
    protected function playSign($params){
        return md5('appId='.self::appId.'time='.$params['time'].'userId='.$params['userId'].self::appKey);
    }

    /**
     * 获取渠道进游戏的url
     * @param $gid
     */
    protected function getGameUrl($gid){

    }

}