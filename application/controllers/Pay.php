<?php
include_once APPPATH.'libraries/http/HttpClient.php';
include_once APPPATH.'libraries/http/HttpHeader.php';
include_once APPPATH.'libraries/http/Request.php';
include_once APPPATH.'libraries/http/Response.php';

/**
 * Created by PhpStorm.
 * User: lincg
 * Date: 11/7/16
 * Time: 2:43 PM
 */

/**
 * Class WeiXinRetCode
 * 威信支付请求返回的状态
 */
abstract class WeiXinRetCode{
    const RET_OK=200;
    const RET_MD5FAILED = 101;
    //Todo 加所有的返回状态
}
/**
 * Class Pay
 * 支付处理
 */
class Pay extends CI_Controller
{
    const BILL_URL='http://pay3.shenzhoufu.com/version3/serverconnwxsm/entry.aspx';
    const BILL_MERID = 123;
    const BILL_VERSION = 3;
    const BILL_CALLBACK_URL = 'pay/callback';
    const BILL_VERIFY_TYPE = 1;
    const BILL_PRIVATE_KEY = 'abc';
    const BILL_PAY_SUCCESS = 1;

    /**
     * 支付请求，返回威信支付的qrcodeurl
     * @param $orderId
     * @param $money
     */
    public function req($orderId){
        $payUrl = '';
        $orderId = intval($orderId);
        if($orderId <= 0 ){
            log_message("warn","orderId little zero");
            redirect("");
        }
        $money = $this->getOrderMoney($orderId);
        $params = array(
            'version' => self::BILL_VERSION,
            'merId' => self::BILL_MERID,
            'payMoney' => $money,
            'orderId' => $this->formatOrderId($orderId),
            'returnUrl' => self::BILL_CALLBACK_URL,
            'privateField' => $this->privateField($orderId),
            'verifyType' => self::BILL_VERIFY_TYPE,
        );
        $params['md5String'] = $this->getMD5String($params);
        $response = HttpClient::get(self::BILL_URL, $params);
        if($response->getStatus() == Response::STATUS_OK) {
            $ret = json_decode($response->getBody(), TRUE);
            if($this->verifyWeiXinPayUrl($ret)){
                if($ret['resCode'] == WeiXinRetCode::RET_OK){
                    $payUrl = $ret['qrCodeUrl'];
                }else{
                    log_message("error", "weixin return error ");
                }
            }
        }
        echo $payUrl;
    }

    public function callback(){
        $version = $this->input->get_post('version');
        $merId = $this->input->get_post('merId');
        $payMoney = $this->input->get_post('payMoney');
        $orderId = $this->input->get_post('orderId');
        $payResult = $this->input->get_post('payResult');
        $md5String = $this->input->get_post('md5String');
        if($this->verifyCallback($version, $merId, $payMoney, $orderId, $payResult, $md5String)){
            if($payResult == self::BILL_PAY_SUCCESS){
                $realOrderId = $this->getRealOrderId($orderId);
                if($realOrderId){
                    if($this->checkOrderIsPayed($realOrderId)){
                        //Todo 处理订单已经完成支付的逻辑,一般无需做处理，只是记录下
                    }else{
                        //Todo 处理订单, 更新订单状态为支付成功

                    }
                }else{
                    //Todo 处理orderId返回的错误
                }
            }else{
                //Todo 处理支付错误的逻辑
            }
        }else{
            //Todo 处理校验错误的逻辑
        }
        $this->callbackReturn($orderId);
    }

    protected function getOrderMoney($orderId){
        //Todo 获取订单的金额, 从数据库获取
    }

    protected function callbackReturn($orderId){
        echo $orderId;
        exit;
    }

    protected function checkOrderIsPayed($orderId){
        //Todo 检查订单是否已经完成支付
    }

    protected function verifyCallback($version, $merId, $payMoney, $orderId, $payResult, $md5String){
        return $md5String == md5($version.$merId.$payMoney.$orderId.$payResult.self::BILL_PRIVATE_KEY);
    }

    protected function verifyWeiXinPayUrl($data){
        $data['md5String'] == md5($data['resCode'].$data['orderId'].$data['qrCodeUrl'].self::BILL_PRIVATE_KEY);
    }

    protected function getMD5String($params){
        return md5($params['version'].$params['merId'].$params['payMoney'].$params['orderId'].$params['returnUrl'].$params['privateField'].$params['verifyType'].self::BILL_PRIVATE_KEY);
    }

    protected function formatOrderId($orderId){
        return date('Ymd').'-'.self::BILL_MERID.'-'.$orderId;
    }

    protected function getRealOrderId($orderString){
        $ret = explode('-', $orderString, 3);
        if(count($ret) != 3)
            return FALSE;
        if($ret[1] != self::BILL_MERID)
            return FALSE;
        return $ret[2];
    }

    protected function privateField($orderId){
        return '';
    }

}
