<?php
/**
 * Created by PhpStorm.
 * User: icemo
 * Date: 14-6-17
 * Time: 下午4:21
 */

/**
 * Class HttpClient
 * HttpClient API
 *
 * <code>
 * $response = HttpClient::get("http://www.xy.com")
 * $response = HttpClient::post("http://www.xy.com")
 * $response = HttpClient::request(new Request("http://www.xy.com"))
 * </code>
 */
abstract class HttpClient {

    static function get($url, $params) {
	    $request = new Request($url, Request::METHOD_GET);
		$request->setParams($params);
        return self::request($request);
    }

    static function post($url, $params) {
        $request = new Request($url, Request::METHOD_POST);
        if(is_array($params) && count($params)) {
            $request->setParams($params);
        }
        return self::request($request);
    }

    static function request(Request $request) {
        $requestHeader[] = "API-RemoteIP: " . $_SERVER['REMOTE_ADDR'];
        foreach($request->getHeaders() as $k => $v) {
            $requestHeader[] = "{$k}:{$v}";
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeader);
        curl_setopt($ch, CURLOPT_URL, $request->getFullURL());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request->getMethod());
        if($request->getMethod() == Request::METHOD_POST) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request->getParam());
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_PORT, $request->getPort());
        curl_setopt($ch, CURLOPT_TIMEOUT, $request->getTimeout());
        $responseBody = curl_exec($ch);
        $response = null;
        if(curl_errno($ch) === 0) {
            $info = curl_getinfo($ch);
            $response = new Response($info['http_code'], $responseBody);
            $response->setHeader(HttpHeader::CONTENT_TYPE, $info['content_type']);
            $response->setExecTime($info['total_time']);
        } else {
            $response = Response::errorResponse(curl_error($ch));
        }
        curl_close($ch);
        return $response;
    }

}
