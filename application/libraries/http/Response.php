<?php
/**
 * Created by PhpStorm.
 * User: icemo
 * Date: 14-6-17
 * Time: 下午4:20
 */

/**
 * Class Response
 * http request 返回结果类
 *
 * <code>
 * $response = HttpClient::request($request)
 * $response->isError()       //是否执行成功
 * $response->getError()      //错误信息
 * $response->getStatus()     //code, 200, 502
 * $response->getCharset()    //返回字符类型
 * $response->getBody()       //返回body内容
 * $response->getExecTime()   //返回执行时间
 * $response->getHeader($key) //返回header内容
 * </code>
 *
 *
 */
class Response {

    const STATUS_OK = 200;

    /**
     * @var header信息
     */
    private $headers;

    /**
     * @var  httpCode
     */
    private $httpCode;

    /**
     * @var body内容
     */
    private $body;

    /**
     * @var 执行时间
     */
    private $execTime;

    /**
     * @var 错误信息
     */
    private $error;

    function __construct($code, $body) {
        $this->httpCode = $code;
        $this->body = $body;
        $this->error = "";
    }

    /**
     * 获取字符信息
     * @return string
     */
    function getCharset() {
        if(strpos($this->headers[HttpHeader::CONTENT_TYPE], "charset")) {
            return substr($this->headers[HttpHeader::CONTENT_TYPE], strpos($this->headers[HttpHeader::CONTENT_TYPE], "charset") + 8);
        } else {
            return "";
        }
    }

    /**
     * 获取body信息
     * @return body
     */
    function getBody() {
        return $this->body;
    }

    /**
     * 获取http返回结果状态
     * @return httpCode
     */
    function getStatus() {
        return $this->httpCode;
    }

    function setHeader($k, $v) {
        $this->headers[$k] = $v;
    }

    /**
     * 返回http response header信息
     * @param $k
     * @param null $default
     * @return null
     */
    function getHeader($k, $default = null) {
        if(isset($this->headers[$k])) {
            return $this->headers[$k];
        } else {
            return $default;
        }
    }

    function setExecTime($time) {
        $this->execTime = $time;
    }

    /**
     * 获取执行时间
     * @return mixed
     */
    function getExecTime() {
        return $this->execTime;
    }

    /**
     * 是否包含错误信息
     * @return bool
     */
    function isError() {
        return !empty($this->error);
    }

    /**
     * 返回错误信息
     * @return string
     */
    function getError() {
        return $this->error;
    }

    function setError($msg) {
        $this->error = $msg;
    }

    /**
     * 构造一个包混错误信息的 Response
     * @param $error
     * @return Response
     */
    static function errorResponse($error) {
        $response = new Response(0, "");
        $response->setError($error);
        return $response;
    }
}

