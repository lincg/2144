<?php
/**
 * Created by PhpStorm.
 * User: icemo
 * Date: 14-6-17
 * Time: 下午4:20
 */

/**
 * http请求描述类，构造http request请求
 *
 * <code>
 * $request = new Request("http://www.xy.com", Request::METHOD_GET);
 * $request->setTimeout(20)
 *         ->setParam('v', '1')
 *         ->setParams(array('v2' => '2'))
 *         ->setPort(8080)
 *         ->setHeader('header1', 'value1')
 *         ->setHeaders(array('header2' => 'value2'))
 * HttpClient::request($request)
 * </code>
 *
 *
 * Class Request
 */

class Request {

    /**
     * 默认超时时间，秒
     */
    const DEFAULT_TIMEOUT = 3;

    CONST METHOD_GET  = "GET";

    const METHOD_POST = "POST";

    const METHOD_PUT  = "PUT";

    /**
     * @var 请求url
     */
    private $url;

    /**
     * @var 请求类型
     */
    private $method;

    /**
     * @var cookie值
     */
    private $cookies;

    /**
     * @var 请求参数
     */
    private $params;

    /**
     * @var 超时时间
     */
    private $timeoutSeconds;

    /**
     * @var 请求header头信息
     */
    private $headers;

    /**
     * @var 请求端口信息
     */
    private $port;

    function __construct($url, $method = "get") {
        $this->url = $url;
        $this->method = $method;
        $this->timeoutSeconds = self::DEFAULT_TIMEOUT;
        $this->port = 0;
        $this->headers = array();
        $this->params  = array();
        $this->cookies = array();
    }

    /**
     * 设置端口
     * @param $port
     * @return $this
     */
    function setPort($port) {
        $this->port = intval($port);
        return $this;
    }

    /**
     * 获取端口
     * @return int
     */
    function getPort() {
        return $this->port;
    }

    /**
     * 设置header信息
     *
     * @param $k
     * @param $v
     * @return $this
     */
    function setHeader($k, $v) {
        $this->headers[$k] = $v;
        return $this;
    }

    /**
     * 设置多个header信息
     *
     * @param $kv
     * @return $this
     */
    function setHeaders($kv) {
        foreach($kv as $k => $v) {
            $this->setHeader($k, $v);
        }
        return $this;
    }

    /**
     * 获取单个header信息或者全部header信息
     *
     * @param null $k
     * @param null $default
     * @return array|null
     */
    function getHeaders($k = null, $default = null) {
        if($k == null) {
            return $this->headers;
        } else {
            if(isset($this->headers[$k])) {
                return $this->headers[$k];
            } else {
                return $default;
            }
        }
    }

    /**
     * 获取请求类型
     * @return string
     */
    function getMethod() {
        return $this->method;
    }

    /**
     * 设置单个请求参数
     * @param $k
     * @param $v
     * @return $this
     */
    function setParam($k, $v) {
        $this->params[$k] = $v;
        return $this;
    }

    /**
     * 设置多个请求参数
     * @param $kv
     * @return $this
     */
    function setParams($kv) {
        foreach($kv as $k => $v) {
            $this->setParam($k, $v);
        }
        return $this;
    }

    /**
     * 获取单个参数信息或者全部参数信息
     * @param null $k
     * @param null $default
     * @return array|null
     */
    function getParam($k = null, $default = null) {
        if($k == null) {
            return $this->params;
        } else {
            if(isset($this->params[$k])) {
                return $this->params[$k];
            } else {
                return $default;
            }
        }
    }


    function setCookie($k, $v) {
        $this->cookies[$k] = $v;
        return $this;
    }

    function setCookies($kvs) {
        foreach($kvs as $k => $v) {
            $this->cookies[$k] = $v;
        }
        return $this;
    }

    function getCookie($k = null, $default = null) {
        if($k == null) {
            return $this->cookies;
        } else {
            if(isset($this->cookies[$k])) {
                return $this->cookies[$k];
            } else {
                return $default;
            }
        }
    }

    /**
     * 设置url
     * @param $url
     * @return $this
     */
    function setURL($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * 获取请求url
     * @return mixed
     */
    function getURL() {
        return $this->url;
    }

    /**
     * 获取完整的url信息
     * @return string
     */
    function getFullURL() {
        if(count($this->params) && $this->getMethod() == self::METHOD_GET) {
            if(strpos("?", $this->url) > 0) {
                return $this->url . "&" . http_build_query($this->params);
            } else {
                return $this->url . "?" . http_build_query($this->params);
            }
        } else {
            return $this->url;
        }
    }

    /**
     * 设置超时时间
     * @param $seconds
     * @return $this
     */
    function setTimeout($seconds) {
        $this->timeoutSeconds = intval($seconds);
        return $this;
    }

    /**
     * 返回超时时间
     * @return int
     */
    function getTimeout() {
        return $this->timeoutSeconds;
    }

}