<?php
/**
 * Created by PhpStorm.
 * User: lincg
 * Date: 10/29/16
 * Time: 11:58 AM
 */

if(!function_exists("web_log")){
    /**
     * extend info web log
     * @param $logs
     * @param $file
     * @param string $level
     */
    function web_log($logs, $file, $level='debug'){
        $log_path = config_item("log_path");
        if(!$log_path)
            $log_path = APPPATH.'logs/'.$file.'-'.date('Ymd').".log";
        if(!is_array($logs)){
            $logs = array(
                'log' => $logs
            );
        }
        $ci = get_instance();
        $logs['ip'] = $ci->input->ip_address();
        $logs['agent'] = $ci->input->user_agent();
        $logs['uri'] = current_url();
        $logs['level'] = $level;
        $logs['time'] = date(config_item('log_date_format'));
        ksort($logs);
        $values = array();
        foreach($logs as $k => $v){
            $values[] = $k.'='.$v;

        }
        $values = implode('|', $values).PHP_EOL;
        file_put_contents($log_path, $values, FILE_APPEND);
    }
}
