<?php $this->load->view('mobile/header');?>
<body>
<div class="container">
  <div class="d-head">
    <a href="#" class="d-fanhui"><img src="<?=static_url('images/d_fanhui.png')?>" width="22" height="22"></a>
    <span class="fs16 col-333">注册</span>
  </div>
  <div class="d-zhuce">
    <form>
      <ul class="d-zcbox">
        <li><input type="text" placeholder="请输入您的手机号码" onblur="checkPhone()" id="phone" required="required" /></li>
        <li class="d-yzmbox"><input type="text" placeholder="请输入验证码"  required="required"/><a href="#" class="d-yzm" id="d-btn">获取验证码</a></li>
        <li><input type="password" placeholder="设置密码：6-20个字符"/></li>
        <li><input type="password" placeholder="再次输入密码" /></li>
        <input type="submit" value="确认" class="d-denglu"/>
      </ul>
    </form>
    
  </div>
</div>
<script type="text/javascript">
window.onload=function(){
var wait=60;
function time(o) {
        if (wait == 0) {
            o.removeAttribute("disabled");          
            o.innerHTML="获取验证码"; 

            wait = 60;
        } else {
            o.setAttribute("disabled", true);
            document.getElementById("d-btn").style.background="#ccc";
            o.innerHTML="重新发送(" + wait + ")";
            wait--;
            setTimeout(function() {
                time(o)
            },
            1000)
        }
    }
document.getElementById("d-btn").onclick=function(){time(this);}
};

    function checkPhone(){ 
    var phone = document.getElementById('phone').value;
    if(!(/^1[34578]\d{9}$/.test(phone))){ 
        alert("手机号码有误，请重填");  
        return false; 
    } 
};
</script>
</body>
<?php $this->load->view('mobile/footer');?>
