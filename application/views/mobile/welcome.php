<?php $this->load->view('mobile/header');?>
<body>
<?php $this->load->view('mobile/nav');?>
<!--轮播图-->
<div class="new_public clearfix">
    <div class="new_tit">
        <p class="tit_ico tit_ico1">我玩过的</p>
    </div>
    
    <div class="meposition">
        <div class="swiper-container swiper-container-horizontal">
            <div class="swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(-1280px, 0px, 0px);">
                <!-- 获取的8条数据 分成2部分div -->               
                <div class="swiper-slide swiper-slide-prev" data-swiper-slide-index="0" >
                    <ul class="new_index_list clearfix">
                    <!-- 加入点击统计 -->
                        <li>
                            <a href="#">
                                <img src="<?=static_url('images/game_pic.png');?>" class="myimg">
                                <span>2点兵三国志</span>
                            </a>
                        </li>
                        </ul>
                </div>
                <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="1" >
                    <ul class="new_index_list clearfix">
                        <!-- 加入点击统计 -->
                        <li>
                           <a  href="#">
                            <img src="<?=static_url('images/game_pic.png')?>" class="myimg">
                            <span>3皇城传奇</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination swiper-pagination-clickable">
            <span class="swiper-pagination-bullet"></span>
            <span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span>
        </div>
    </div>
</div>
<!---->
</body>
<script type="text/javascript" src="<?=static_url('js/swiper.js')?>"></script>
<script type="text/javascript">
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplayDisableOnInteraction : false,
        speed:800,
        autoplay : 3000,
        loop : true,

    });
</script>
<?php $this->load->view('mobile/footer');?>
