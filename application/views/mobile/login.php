<?php $this->load->view('mobile/header');?>
<body>
<div class="container">
  <div class="d-head">
    <a href="#" class="d-fanhui"><img src="<?=static_url('images/d_fanhui.png')?>" width="22" height="22"></a>
    <span class="fs16 col-333">登录</span>
  </div>
  <div class="d-zhuce">
    <form>
      <ul class="d-zcbox">
        <li class="d-user"><input type="text" onblur="checkPhone()" id="phone" placeholder="请输入您的手机号码" required="required" /></li>
        <li class="d-password"><input type="password" placeholder="请输入您的密码" /></li>
        <div class="d-find">
           <p><a href="#" class="d-zc">注册</a><a href="#" class="fr">找回密码</a></p>
        </div>
        <input type="submit" value="登录" class="d-denglu"/>
      </ul>
    </form>
    
  </div>
</div>
<script>
    function checkPhone(){ 
    var phone = document.getElementById('phone').value;
    if(!(/^1[34578]\d{9}$/.test(phone))){ 
        alert("手机号码有误，请重填");  
        return false; 
    } 
}
</script>
</body>
<?php $this->load->view('footer');?>
