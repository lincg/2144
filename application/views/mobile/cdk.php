<?php $this->load->view('mobile/header');?>
<body>
<?php $this->load->view('mobile/nav');?>
<!--title-->
<div class="title-box">
    <p class="title-on">
      <a href="#">礼包</a>
      <i class="blue-triangle"></i> 
    </p>
    <p>
        <a href="#">开服</a>
        <i class="blue-triangle"></i>
    </p>
</div>
<div class="game-con">
    <ul class="game-box">
        <li>
            <a href="#" class="a-flex">
                <img src="images/game_pic.png" class="game-pic">
                <div class="game-info">
                    <ul>
                        <li class="col-333 fs14">烈焰遮天：<span class="col-red">圣诞节礼包</span></li>
                        <li class="col-999 fs14 libao-time">开始时间：<span class="col-red">2016-10-31&nbsp;&nbsp;00:00</span></li>
                    </ul>
                </div>
                <button class="game-btn no-begin">未开始</button>
            </a>
        </li>
    </ul>
    <ul class="game-box">
        <li>
            <a href="#" class="a-flex">
                <img src="images/game_pic.png" class="game-pic">
                <div class="game-info">
                    <ul>
                        <li class="col-333 fs14">烈焰遮天：<span class="col-red">圣诞节礼包</span></li>
                        <li class="col-999 fs14 libao-time">开始时间：<span class="col-red">2016-10-31&nbsp;&nbsp;00:00</span></li>
                    </ul>
                </div>
                <button class="game-btn no-begin">未开始</button>
            </a>
        </li>
    </ul>
</div>

<div class="game-con kf-bor-left" style="display: none">
    <div class="kf-today">
    <p class="kf-today-p">今天</p>
    <ul class="game-box">
        <li><a href="#" class="a-flex">
            <img src="images/game_pic.png" class="game-pic">
            <div class="game-info">
                <ul>
                    <li class="col-333 fs16">烈焰遮天</li>                   
                    <li class="col-999 fs14">开服时间：<span class="col-red">08:00</span></li>
                    <li class="col-999 fs14">服务器名：<span class="col-red">9区</span></li>
                </ul>
            </div>
            <button class="game-btn">领取礼包</button><button class="game-btn">开始玩</button>
        </a></li>
    </ul>
    <ul class="game-box last-one">
        <li><a href="#" class="a-flex">
            <img src="images/game_pic.png" class="game-pic">
            <div class="game-info">
                <ul>
                    <li class="col-333 fs16">烈焰遮天</li>                    
                    <li class="col-999 fs14">开服时间：<span class="col-red">08:00</span></li>
                    <li class="col-999 fs14">服务器名：<span class="col-red">9区</span></li>
                </ul>
            </div>
            <button class="game-btn">领取礼包</button><button class="game-btn">开始玩</button>
        </a></li>
    </ul>
    </div>

    <div class="kf-today kf-jijiang">
    <p class="kf-today-p kf-jijiang-p">即将</p>
    <ul class="game-box">
        <li><a href="#" class="a-flex">
            <img src="images/game_pic.png" class="game-pic">
            <div class="game-info">
                <ul>
                    <li class="col-333 fs16">烈焰遮天</li>                    
                    <li class="col-999 fs14">开服时间：<span class="col-red">08:00</span></li>
                    <li class="col-999 fs14">服务器名：<span class="col-red">9区</span></li>
                </ul>
            </div>
            <button class="game-btn">领取礼包</button><button class="game-btn">开始玩</button>
        </a></li>
    </ul>
    <ul class="game-box last-one">
        <li><a href="#" class="a-flex">
            <img src="images/game_pic.png" class="game-pic">
            <div class="game-info">
                <ul>
                    <li class="col-333 fs16">烈焰遮天</li>                    
                    <li class="col-999 fs14">开服时间：<span class="col-red">08:00</span></li>
                    <li class="col-999 fs14">服务器名：<span class="col-red">9区</span></li>
                </ul>
            </div>
            <button class="game-btn">领取礼包</button><button class="game-btn">开始玩</button>
        </a></li>
    </ul>
    </div>

    <div class="kf-today kf-already">
    <p class="kf-today-p kf-already-p">已经开服</p>
    <ul class="game-box">
        <li><a href="#" class="a-flex">
            <img src="images/game_pic.png" class="game-pic">
            <div class="game-info">
                <ul>
                    <li class="col-333 fs16">烈焰遮天</li>                    
                    <li class="col-999 fs14">开服时间：<span class="col-red">08:00</span></li>
                    <li class="col-999 fs14">服务器名：<span class="col-red">9区</span></li>
                </ul>
            </div>
            <button class="game-btn">领取礼包</button><button class="game-btn">开始玩</button>
        </a></li>
    </ul>
    <ul class="game-box last-one">
        <li><a href="#" class="a-flex">
            <img src="images/game_pic.png" class="game-pic">
            <div class="game-info">
                <ul>
                    <li class="col-333 fs16">烈焰遮天</li>                    
                    <li class="col-999 fs14">开服时间：<span class="col-red">08:00</span></li>
                    <li class="col-999 fs14">服务器名：<span class="col-red">9区</span></li>
                </ul>
            </div>
            <button class="game-btn">领取礼包</button><button class="game-btn">开始玩</button>
        </a></li>
    </ul>
    </div>
</div>
<div class="already-bom">
    已经到底了...
</div>
<!--top-->
<div class="go-top">
    TOP
</div>
<!---->
    <script>
        $(function(){
            $('.title-box p').click(function(){
                $(this).addClass('title-on').siblings().removeClass('title-on');
                $('.game-con').hide();
                $('.game-con').eq($(this).index()).show();
            });
        });

        $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
           $(".go-top").fadeIn(400);//当滑动栏向下滑动时，按钮渐现的时间
        } else {
          $(".go-top").fadeOut(200);//当页面回到顶部第一屏时，按钮渐隐的时间
        }
        });
        $(".go-top").click(function () {
        $('html,body').animate({
        scrollTop : '0px'
        }, 200);//返回顶部所用的时间 
      });
    </script>
</body>
<?php $this->load->view('mobile/footer');?>
